import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default (state = { isOwner: initialState.isOwner, message: "" }, action) => {
  switch (action.type) {
    case types.LOGIN_REQUEST:
      return {
        ...state,
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state, isOwner: true,
      };
    case types.LOG_OUT:
      return {
        ...state, isOwner: false,
      };
    case types.LOGIN_FAILED:
      return {
        ...state, isOwner: false, message: action.err.message,
      };
    default:
      return state;
  }
};
