import statementsReducer from './statementsReducer';
import * as types from '../actions/actionTypes';

describe('statementsReducer', () => {
  let initialState;
  let action;
  beforeEach(() => {
    initialState = {
      transactions: {},
    };
    action = { type: types.LOAD_TRANSACTIONS_SUCCESS, data: [{ 'beneficary': 'Zara', 'dateStr': '19 April 2010', 'value': '£12' }, { 'beneficary': 'Tesco', 'dateStr': '20 April 2012', 'value': '£32' }] };
  });
  it('adds transactions by date', () => {
    expect(statementsReducer(initialState, action)).toEqual({
      ...initialState,
      transactions: {
        "19 April 2010": [
          { "beneficary": 'Zara', 'value': '£12', dateStr: "19 April 2010", },
        ],
        "20 April 2012": [
          { "beneficary": 'Tesco', 'value': '£32', dateStr: "20 April 2012", },
        ],
      },
    });
  });
});
