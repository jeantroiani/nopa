import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form'
import bankReducer from './bankReducer';
import userReducer from './userReducer';
import statementsReducer from './statementsReducer';

const rootReducer = combineReducers({
  bank: bankReducer,
  user: userReducer,
  statements: statementsReducer,
  form: reduxForm
});

export default rootReducer;
