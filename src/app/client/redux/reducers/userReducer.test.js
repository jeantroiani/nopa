import userReducer from './userReducer';
import * as types from '../actions/actionTypes';

describe('userReducer', () => {
  let initialState;
  let action;
  beforeEach(() => {
    initialState = {
      isOwner: false,
      message: "",
    };
    action = { type: types.LOGIN_SUCCESS };
  });

  it('when successfully logged flags user as owner', () => {
    expect(userReducer(initialState, action)).toEqual({
      ...initialState,
      isOwner: true,
      message: '',
    });
  });

  it('when failing log in flags is owner shows message', () => {
    action = { type: types.LOGIN_FAILED, err: { message: "¯\_(ツ)_/¯" } };
    expect(userReducer(initialState, action)).toEqual({
      ...initialState,
      isOwner: false,
      message: "¯\_(ツ)_/¯",
    });
  });
});
