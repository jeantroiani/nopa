import * as types from '../actions/actionTypes';
import initialState from './initialState';
import { splitBy } from '../../helpers/common';

export default (state = { transactions: {}}, action) => {
  switch (action.type) {
    case types.LOAD_TRANSACTIONS_REQUEST:
      return {
        ...state,
      };
    case types.LOAD_TRANSACTIONS_SUCCESS:
      const data = splitBy(action.data, 'dateStr');
      return {
        ...state, transactions: data,
      };
    case types.LOAD_TRANSACTIONS_FAILED:
      return {
        ...state,
    };
    default:
      return state;
  }
};
