import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default (state = { name: initialState.bank }, action) => {
  switch (action.type) {
    case types.BANK_SELECT:
      return {
        ...state, name: action.value,
      };
    default:
      return state;
  }
};
