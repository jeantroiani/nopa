import bankReducer from './bankReducer';
import * as types from '../actions/actionTypes';

describe('bankReducer', () => {
  let initialState;
  let action;
  beforeEach(() => {
    initialState = {
      name: ""
    };
    action = {type: types.BANK_SELECT, value: 'MetroBank'};
  })
  it('updates bank name on the state', () => {
    expect(bankReducer(initialState, action)).toEqual({
      ...initialState,
      name: "MetroBank"
    });
  })
})
