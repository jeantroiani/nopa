import { bankSelect, userLogin, loadTransactions, logOut } from './actionTypes';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import { isomorphicFetch as fetch } from 'isomorphic-fetch';
import * as router from 'react-router';
import * as types from '../actions/actionTypes';
import sinon from 'sinon';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions', () => {

  describe('bankSelect', () => {
    it('returns an action type', () => {
      expect(bankSelect().type).toEqual(types.BANK_SELECT);
    })
  });

  describe('logOut', () => {
    it('returns a log out action', () => {
      expect(logOut().type).toEqual(types.LOG_OUT);
    })
  });

  describe('userLogin', () => {
    afterEach(() => { nock.cleanAll() });
    it('creates LOGIN_SUCCESS when user login request is completed', () => {
      nock('http://localhost:3000/')
        .get('/api/transactions')
        .reply(200,  { "beneficary": "Zara" });

        router.browserHistory = { push: ()=>{} };
        let browserHistoryPushStub = sinon.stub(router.browserHistory, 'push', () => { });

      const expectedActions = [
        { type: types.LOGIN_REQUEST },
        { type: types.LOGIN_SUCCESS, response: {"beneficary": "Zara" } }
      ];
      const store = mockStore({ transactions: [] });

      return store.dispatch(userLogin())
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions);
        });
      browserHistoryPushStub.restore();
    });

    it('creates LOGIN_FAILED when log in attempt throws', () => {

      nock('http://localhost:3000/')
        .get('/api/transactions')
        .replyWithError({'message': 'Boom!'});

      const expectedActions = [
        { type: types.LOGIN_REQUEST },
        { type: types.LOGIN_FAILED }
      ];
      const store = mockStore({ transactions: [] });

      return store.dispatch(userLogin())
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions)
        })
    })
  });

  describe('loadTransactions', () => {
    afterEach(() => { nock.cleanAll() });
    it('creates LOAD_TRANSACTIONS_SUCCESS when loading transactions is completed', () => {
      nock('http://localhost:3000/')
        .get('/api/transactions')
        .reply(200,  { "beneficary": "Zara" });

      const expectedActions = [
        { type: types.LOAD_TRANSACTIONS_REQUEST },
        { type: types.LOAD_TRANSACTIONS_SUCCESS, data: {"beneficary": "Zara"} }
      ];
      const store = mockStore({ transactions: [] });

      return store.dispatch(loadTransactions())
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions)
        })
    });

    it('creates LOAD_TRANSACTIONS_FAILED when loading transactions throws', () => {

      nock('http://localhost:3000/')
        .get('/api/transactions')
        .replyWithError({'message': 'Boom!',});

      const expectedActions = [
        { type: types.LOAD_TRANSACTIONS_REQUEST },
        { type: types.LOAD_TRANSACTIONS_FAILED}
      ];
      const store = mockStore({ transactions: [] });

      return store.dispatch(loadTransactions())
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions)
        })
    })
  });
});
