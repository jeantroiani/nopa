import { browserHistory } from 'react-router'
import { makeRequest } from '../../helpers/common'

export const BANK_SELECT = 'BANK_SELECT';
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const LOG_OUT = 'LOG_OUT';
export const LOAD_TRANSACTIONS_REQUEST = 'LOAD_TRANSACTIONS_REQUEST';
export const LOAD_TRANSACTIONS_SUCCESS = 'LOAD_TRANSACTIONS_SUCCESS';
export const LOAD_TRANSACTIONS_FAILED = 'LOAD_TRANSACTIONS_FAILED';

export function bankSelect(value) {
  return { type: BANK_SELECT, value };
}

export function userLogin(userData) {
  return async(dispatch, getState) => {
    dispatch({ type: LOGIN_REQUEST });
    try {
      const response = await makeRequest('http://localhost:3000/api/transactions');
        dispatch({ type: LOGIN_SUCCESS, response });
        browserHistory.push('/statement');
    } catch (err) {
      dispatch({ type: LOGIN_FAILED });
      console.error(`Error: ${err.message}`);
    }
  }
}

export function logOut() {
  return { type: LOG_OUT };
}

export function loadTransactions(value) {
  return async (dispatch) => {
    dispatch({ type: LOAD_TRANSACTIONS_REQUEST });
    try {
      const data = await makeRequest('http://localhost:3000/api/transactions');
      dispatch({ type: LOAD_TRANSACTIONS_SUCCESS, data })
    } catch (err) {
      dispatch({ type: LOAD_TRANSACTIONS_FAILED })
      console.error(`Error: ${err.message}`);
    }
  }
}
