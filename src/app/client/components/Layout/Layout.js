import React, { Component } from 'react'
import { Header, Footer, Partners } from '../'
import { isNotMobile } from '../../helpers/matchQueries';

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = { listOfURL: [undefined, 'statement']};
  }

  componentDidMount() {
    const { title } = this.props;
    document.title = title;
  }

  isURLOnList = () => {
    const { location } = this.props;
    const { listOfURL } = this.state;
    return listOfURL.includes(location);
  }

  renderConditional = () => {
    const { location } = this.props;
    const { isURLOnList } = this;
    return isNotMobile() && isURLOnList(location)
  };

  render() {
    const { location, children, isOwner, logOut } = this.props;
    const { renderConditional } = this;
    return (
      <section className="layout-section">
        <Header location={ location } isOwner={ isOwner } logOut={ logOut } />
        <div className="layout-section-content">
          {children}
        </div>
        <div className="layout-footer-group">
        <div className={ !renderConditional() ? 'layout-section-partners' : null }>
          <Partners/>
        </div>
        <Footer/>
        </div>
      </section>
    );
  }
}

Layout.propTypes = {
  title: React.PropTypes.string.isRequired,
  isOwner: React.PropTypes.bool,
  logOut: React.PropTypes.func,
};

export default Layout
