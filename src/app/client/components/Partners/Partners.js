import React from 'react';

const partners = [
  {
    name: 'Airbnb',
    logo: require('../../../static/images/Airbnb.png'),
  }, {
    name: 'Metro',
    logo: require('../../../static/images/Metro.png'),
  }, {
    name: 'Pariti',
    logo: require('../../../static/images/Pariti.png'),
  }, {
    name: 'Unshackled',
    logo: require('../../../static/images/Unshackled.png'),
  },
];

const partnerLogos = partners.map((partner, index) => <li key={index} className="partners-list-item"><img className="Nopa" alt={partner.name} src={partner.logo} /></li>);

const Partners = (props) => {
  const { location } = props;
  return (
    <section className="partners-section">
      {location}
      <p className="section-subtitle">Our partners:</p>
      <ul className="partners-list">
        {partnerLogos}
      </ul>
    </section>
  );
};

Partners.propTypes = {
  location: React.PropTypes.string,
};

export default Partners;
