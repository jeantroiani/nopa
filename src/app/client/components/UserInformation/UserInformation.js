import React from 'react';

const UserInformation = (props) => {
  const { bankName, surname, accountNumber, sortcode } = props;
  return (
    <section className="section-user-information">
      <div className="section-user-information-column">
        <p className="section-user-information-content">{ bankName }</p>
        <p className="section-user-information-content">{ surname }</p>
      </div>
      <div className="section-user-information-column">
        <p className="section-user-information-content">Current account</p>
        <p className="section-user-information-content">{ accountNumber }</p>
        <p className="section-user-information-content">{ sortcode }</p>
      </div>
    </section>
  );
};

UserInformation.propTypes = {
  bankName: React.PropTypes.string.isRequired,
  surname: React.PropTypes.string.isRequired,
  accountNumber: React.PropTypes.string.isRequired,
  sortcode: React.PropTypes.string.isRequired,
};

export default UserInformation;
