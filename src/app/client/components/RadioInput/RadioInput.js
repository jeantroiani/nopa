import React from 'react';
import { Field } from 'redux-form';

const RadioInput = (props) => {
  const { name } = props;
  return (
    <div className="radio-input-group">
      <Field
        className="radio-input-radio"
        id={name}
        name="bank"
        type="radio"
        placeholder={name}
        value={name}
        component="input"
        />
      <label className="radio-input-label" htmlFor={name}>
        <span className="radio-input-label-content">{name}</span>
        <span className={`radio-input-image__${name.toLowerCase()}`} />
      </label>
    </div>
  );
};

RadioInput.propTypes = {
  name: React.PropTypes.string.isRequired,
};

export default RadioInput;
