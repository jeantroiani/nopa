import React from 'react';
import { Field } from 'redux-form';

const TextInput = (props) => {
  const { label, input, meta: { error, touched }, type } = props;
  return (
    <div className="form-input">
      <label htmlFor={label}>{label}</label>
      <input id={label} {...input} placeholder={label} type={type} />
      <p className="text-input-error" >{touched && error && error}</p>
    </div>
  );
};

TextInput.propTypes = {
  label: React.PropTypes.string.isRequired,
};

export default TextInput;
