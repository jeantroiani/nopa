import React from 'react';
import { reduxForm } from 'redux-form';
import { RadioInput } from '../';
import * as Paths from '../../constants/paths';
import validate from '../../helpers/validation/rules';

const ChooseBankForm = (props) => {
  const { bankList, handleSubmit } = props;
  return (
    <form className="choose-bank-form" onSubmit={handleSubmit}>
      { bankList.map((bank, index) => <RadioInput {...bank} name="bank" logo={bank.logo} name={bank.name} key={bank.name} />
      )}
      <input className="choose-bank-form-action-container" type="submit" value="Continue" />
    </form>
  );
};

ChooseBankForm.propTypes = {
  handleSubmit: React.PropTypes.func.isRequired,
  bankList: React.PropTypes.array.isRequired,
};

export default reduxForm({ form: 'bank', validate })(ChooseBankForm);
