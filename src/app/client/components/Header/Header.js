import React, { Component } from 'react';
import Button from '../Button/Button';
import * as Paths from '../../constants/paths';
import { isNotMobile } from '../../helpers/matchQueries';
const nopaLogo = require('../../../static/images/Logo_Nopa.svg');

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { listOfURL: [undefined] };
  }

  isURLOnList = (location) => {
    const { listOfURL } = this.state;
    return listOfURL.includes(location);
  }

  handleAction = (e) => {
    const { logOut } = this.props;
    logOut();
  }

  renderConditional = (location) => {
    const { isURLOnList } = this;
    return isNotMobile() && isURLOnList(location);
  };

  render() {
  const { location, isOwner } = this.props;
  const { handleAction, renderConditional, isURLOnList } = this;
    return (
        <div className={ renderConditional(location) ? "header-section__no-logo" : "header-section" }>
          <div className={ renderConditional(location) ? "header-section-action" : null }>
            <a href="/" title="Return to the homepage" className="logo-link">
              <img src={ nopaLogo } alt="Nopa" className="header-section-logo"/>
            </a>
          </div>
          <div className="header-action">
            { isOwner ? <Button onClick={ handleAction } to={ Paths.HOME }>Log Out</Button> : <Button to={ Paths.LOGIN_BANK }>Log In</Button> }
          </div>
        </div>
    )
  };
}

export default Header;


