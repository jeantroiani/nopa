import React from 'react';
import moment from 'moment';
import { splitBy } from '../../helpers/common';

const replaceDateWith = (date, customString) => {
  const timeNow = moment().format("YYYYMMDD");
  const queryDate = moment(date).format("YYYYMMDD");
  return timeNow == queryDate ? customString : date;
};

const calculateDays = (transactions) => {
  const datesEntry = Object.keys(transactions);
  const oldestDateEntry = datesEntry.length - 1;
  return moment(datesEntry[oldestDateEntry], "DD MMMM YYYY").fromNow();
};
const getKeys = (collection) => {
  return Object.keys(collection);
};

const UserTransactions = (props) => {
  const { transactions } = props;

  return (
    <section className="section-user-transactions">
      <p className="section-content">Your transactions for the last { calculateDays(transactions) }</p>
      <div className="section-user-transactions-list">
        {Object.keys(transactions).map((date, id) => {
          return (
            <dl key={id} className="section-user-transactions-list">
              <p className="section-user-transactions-date">{ replaceDateWith(date, "Today") }</p>
              {transactions[date].map((transaction, index) => {
                const { beneficary, value } = transaction;
                return (
                  <div key={index} className="section-user-transaction-group">
                    <dt className="section-user-transaction-beneficary">{beneficary}</dt>
                    <dd className="section-user-transaction-value">{value}</dd>
                  </div>
                );
              })}
            </dl>
          );
        })
      }
      </div>
    </section>
  );
};

UserTransactions.propTypes = {
  transactions: React.PropTypes.object.isRequired,
};

export default UserTransactions;
