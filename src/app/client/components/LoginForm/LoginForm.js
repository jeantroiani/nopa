import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { TextInput } from '../';
import validate from '../../helpers/validation/rules';

const LoginForm = (props) => {
  const { handleSubmit } = props;
  return (
    <form onSubmit={handleSubmit} className="login-form-section">
      <Field name="surname" type="text" component={TextInput} label="Surname" />
      <Field name="sortCode" type="text" component={TextInput} label="Sort code" />
      <Field name="accountNumber" type="text" component={TextInput} label="Account number" />
      <Field name="passcode" type="password" component={TextInput} label="Passcode" />
      <Field name="memorableWord" type="text" component={TextInput} label="Memorable Word" />
      <input className="button" type="submit" value="Login &amp; connect" />
    </form>
  );
};

LoginForm.propTypes = {
  handleSubmit: React.PropTypes.func.isRequired,
};

export default reduxForm({ form: 'login', validate })(LoginForm);
