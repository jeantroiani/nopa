import React, { Component } from 'react';
import { connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { Layout, LoginForm } from '../../components';
import { withRouter } from 'react-router';
import * as Paths from '../../constants/paths';
import { userLogin, bankSelect, logOut } from '../../redux/actions/actionTypes';

export class LoginPage extends Component {
  constructor(props) {
    super(props);
  }

  submit = (userDetails) => {
    const { userLogin } = this.props;
    userLogin(userDetails);
  }

  render() {
    const {route: { title },location: { pathname }, isOwner, logOut } = this.props
    const { submit } = this;
    return (
      <Layout location={ pathname } title={ title } isOwner={ isOwner } logOut={ logOut }>
        <div className="main-content">
          <h1 className="section-main-title">Log in to your online banking</h1>
          <p className="section-content">Enter the same details you use to login to your online banking</p>
          <LoginForm onSubmit={ submit } />
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  const { bank, user } = state;
  return {
    isOwner: user.isOwner,
    bankName: bank.name
  }
}

const mapDispatchToProps = (dispatch) => {
  const actions = {
    userLogin,
    logOut
  }
  return bindActionCreators({
    ...actions
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LoginPage));
