import React from 'react';
import { ChooseBankPage } from './ChooseBankPage';
import ReactShallowRenderer from 'react-test-renderer/shallow';

const initialProps = {
  route: {
    title: "The Title"
  },
  location: {
    pathname: "/path"
  },
  isOwner: true,
  logOut: () => {},
  store: {}
}

test('Renders correctly', () => {
  const renderer = new ReactShallowRenderer();
  renderer.render(<ChooseBankPage { ...initialProps }/>);
  const result = renderer.getRenderOutput();
  expect(result).toMatchSnapshot();
});
