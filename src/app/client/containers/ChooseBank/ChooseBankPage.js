import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Layout, Button, ChooseBankForm } from '../../components';
import * as Paths from '../../constants/paths';
import { bankSelect, logOut } from '../../redux/actions/actionTypes';

const bankList = [
  {
    name: 'Barclays',
    logo: require('../../../static/images/Barclays.png')
  }, {
    name: 'Natwest',
    logo: require('../../../static/images/LogoNatwest.png')
  }, {
    name: 'Lloyds',
    logo: require('../../../static/images/LogoLloyds.png')
  }, {
    name: 'HSBC',
    logo: require('../../../static/images/LogoHSBC.png')
  }, {
    name: 'TSB',
    logo: require('../../../static/images/LogoTSB.png')
  }, {
    name: 'Santander',
    logo: require('../../../static/images/LogoSantander.png')
  }
];

export class ChooseBankPage extends Component {
  constructor(props) {
    super(props);
  }

  submit = (bank) => {
    const { bankSelect, router: { push } } = this.props;
    push(Paths.LOGIN_BANK);
    return bankSelect(bank.bank);
  }

  render() {
    const {route: { title }, location: { pathname }, isOwner , logOut} = this.props;
    return (
      <Layout location={pathname} title={title} isOwner={ isOwner } logOut={ logOut }>
        <div className="main-content">
          <h1 className="section-main-title">Which bank does this account belong to?</h1>
          <p className="section-content">Choose your bank</p>
          <ChooseBankForm
            bankList={bankList}
            bankSelect={this.props.bankSelect}
            title={title}
            onSubmit={this.submit}/>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  const { bank, user } = state;
  return {
    isOwner: user.isOwner,
    bankName: bank.name
  }
}

const mapDispatchToProps = (dispatch) => {
  const actions = {
    bankSelect,
    logOut
  }
  return bindActionCreators({
    ...actions
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ChooseBankPage));
