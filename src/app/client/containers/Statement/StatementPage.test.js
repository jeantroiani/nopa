import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import { StatementPage } from './StatementPage';

const initialProps = {
  route: {
    title: "The Title",
  },
  location: {
    pathname: "/path",
  },
  bankName: "Barclays",
  isOwner: true,
  logOut: () => {},
};

test('Renders correctly', () => {
  const renderer = new ReactShallowRenderer();
  renderer.render(<StatementPage {...initialProps} />);
  const result = renderer.getRenderOutput();

  expect(result).toMatchSnapshot();
});
