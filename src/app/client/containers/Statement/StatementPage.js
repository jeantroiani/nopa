import React, { Component } from 'react';
import { connect } from 'react-redux';
import  moment  from 'moment';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import { Layout, UserInformation, UserTransactions, Button } from '../../components';
import { loadTransactions, bankSelect, userLogin, logOut } from '../../redux/actions/actionTypes';

export class StatementPage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { loadTransactions } = this.props;
    loadTransactions();
  }

  handleClick = (e) => {
    e.preventDefault();
  }

  render() {
    const { route: { title }, location: { pathname }, transactions, bankName, isOwner, logOut } = this.props
    const { handleClick } = this;
    return (
      <section className="section-statement" >
        <Layout location={ pathname } title={ title } isOwner={ isOwner } logOut={ logOut }>
          <h1 className="section-statement-title">Which bank does this account belong to?</h1>
          <p className="section-statement-content">Track all of your payments by connecting as many bank accounts as<br/>
          you'd like to your Nopa account and get updates on your balance instantly. Plus it's free</p>
          <div className="section-statement-user-information">
            <UserInformation
            bankName={ bankName }
            surname="jean"
            accountNumber="12345678"
            sortcode="123456"/>
          </div>
          <UserTransactions transactions={ transactions }/>
          <div className="section-statement-action-group">
            <Button className="button" onClick={ handleClick }>Show more</Button>
          </div>
        </Layout>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  const { bank, statements, user } = state;
  return {
    isOwner: user.isOwner,
    bankName: bank.name,
    transactions: statements.transactions
  }
}

const mapDispatchToProps = (dispatch) => {
  const actions = {
    loadTransactions,
    logOut
  }
  return bindActionCreators({
    ...actions
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(StatementPage));
