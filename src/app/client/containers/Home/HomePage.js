import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router';
import { Layout, Button } from '../../components';
import * as Paths from '../../constants/paths';
import { logOut } from '../../redux/actions/actionTypes';

export const HomePage = (props) => {
  const { isOwner, logOut } = props;
  return (
    <Layout className="section-home" title="Welcome to Nopa!" isOwner={isOwner} logOut={logOut}>
      <div className="main-content">
        <div className="homepage-logo">
          <Link to={Paths.HOME} title="Home" className="">
            <img className="Nopa" alt="Logo" src={require('../../../static/images/Logo_Nopa.svg')} />
          </Link>
        </div>
        <h1 className="section-main-title">Your finances, in one place</h1>
        <p className="section-content">Track all of your payments by connecting as many bank accounts as you'd like to your Nopa account and get updates on your balance instantly.</p>
        <div className="section-home-action-group">
          <Button to={Paths.CHOOSE_BANK} className="button">Get started</Button>
        </div>
      </div>
      <div className="secondary-content">
        <div className="secondary-content-container">
          <div className="section-home-content-container">
            <h1 className="section-main-title__blue">There's no such things as "one size fits all" finance.</h1>
            <p className="section-content__grey">We were founded to make money simple and fair for everyone: whether you're looking for a loan, or to get better rewards for your investments.</p>
          </div>
          <div className="section-home-image-container">
            <img className="section-home-image" alt="Shapes" src={require('../../../static/images/Shapes.png')} />
          </div>
        </div>
      </div>
    </Layout>
  );
};

const mapStateToProps = (state) => {
  const { user } = state;
  return {
    isOwner: user.isOwner,
  };
};

const mapDispatchToProps = (dispatch) => {
  const actions = {
    logOut,
  };
  return bindActionCreators({ ...actions }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(HomePage));
