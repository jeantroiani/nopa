import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import { ErrorPage } from './ErrorPage';

const initialProps = {
  route: {
    title: "The Title",
  },
  location: {
    pathname: "/path",
  },
  bankName: "Barclays",
  isOwner: true,
  logOut: () => {},
};

test('Renders correctly', () => {
  const renderer = new ReactShallowRenderer();
  renderer.render(<ErrorPage {...initialProps} route={{ error: '404' }} />
  );
  const result = renderer.getRenderOutput();
  expect(result).toMatchSnapshot();
});
