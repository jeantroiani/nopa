import React from 'react';
import { Layout } from '../../components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router';
import * as Paths from '../../constants/paths';
import { logOut } from '../../redux/actions/actionTypes';

const errors = {
  '500': {
    title: 'Server error!',
    desc: 'We have been notified about this error'
  },
  '404': {
    title: 'Page not found!',
    desc: 'This page is either no longer available or you have entered a wrong path.'
  }
};

export class ErrorPage extends React.Component {
  printError = (errCode) => {
    const error = Object.keys(errors).indexOf(errCode) > -1 ? errors[errCode] : errors['500'];
    return (
      <div className="section-error-content-group">
          <div className="homepage-logo">
            <Link to={Paths.HOME} title="Home" className="">
              <img className="Nopa" alt="Logo" src={ require('../../../static/images/Logo_Nopa.svg') } />
            </Link>
          </div>
        <h1 className="section-main-title">{ error.title }</h1>
        <div className="section-content">{ error.desc }</div>
      </div>
    );
  }

  render () {
    const { route: { title } } = this.props;
    const { printError } = this;
    return (
      <section className="section-error-page">
        <Layout title={ title } >
          { printError(this.props.route.error) }
        </Layout>
      </section>
    );
  }
}

ErrorPage.propTypes = {
  route: React.PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  const { user } = state;
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  const actions = {
  }
  return bindActionCreators({
    ...actions
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ErrorPage));

