const validate = (values) => {
  const errors = {};
  if (!values.surname) {
    errors.surname = 'Required';
  } else if (values.surname.length > 15) {
    errors.surname = 'Must be 15 characters or less';
  }
  if (!values.sortCode) {
    errors.sortCode = 'Required';
  }

  if (values.sortCode && values.sortCode.replace(/-|\s/g, "").length !== 6) {
    errors.sortCode = 'Invalid sort code number';
  }

  if (values.sortCode && !/^[0-9]+$/.test(values.sortCode)) {
    errors.sortCode = 'Only numbers are accepted';
  }

  if (!values.accountNumber) {
    errors.accountNumber = 'Required';
  }

  if (values.accountNumber && values.accountNumber.replace(/-|\s/g, "").length !== 8) {
    errors.accountNumber = 'Invalid account number';
  }

  if (values.accountNumber && !/^[0-9]+$/.test(values.accountNumber)) {
    errors.accountNumber = 'Only numbers are accepted';
  }

  if (!values.passcode) {
    errors.passcode = 'Required';
  }

  if (!values.memorableWord) {
    errors.memorableWord = 'Required';
  }

  if (!values.bank) {
    errors.bank = 'Required';
  }
  return errors;
};
export default validate;
