export const isNotMobile = () => {
  return window
    .matchMedia("(min-width: 360px)")
    .matches && true;
};
