import moment from 'moment';

const normalizeDate = (date) => {
  if (date === 'Now') date = moment().format('DD MMMM YYYY');
  return moment(date).format('DD MMMM YYYY');
}

export async function makeRequest(url, ...options) {
  const config = {
    method: 'get',
    mode: 'cors',
    cache: 'default'
  };
  const response = await fetch(url, config);
  const data = await response.json();
  return data;
}

export function splitBy(group , property){
	let newCollection = {};
	group.forEach(el => {
    el[property] = normalizeDate(el[property]);
		newCollection[el[property]] = newCollection[el[property]] || [];
		newCollection[el[property]].push(el);
	});
	return newCollection;
}
